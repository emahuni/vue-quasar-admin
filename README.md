<p align="center">
    <a href="https://quasar-framework.org">
        <img width="200" src="https://quasar-framework.org/images/logo/xxhdpi.png">
    </a>
</p>

## vue-quasar-admin
&emsp;&emsp;[Quasar-Framework](https://quasar-framework.org/) is an open source front-end framework based on vue.js that helps web developers quickly create the following websites: responsive websites. Progressive applications, mobile applications (via Cordova), cross-platform applications (via Electron).
&emsp;&emsp;Quasar allows developers to publish to multiple platform websites with only one code at a time, PWA, Mobile App and Electron App. When using Quasar, you don't even need Hammerjs, Momentjs, or Bootstrap, within the Quasar framework. Contains these things already, you can use them very simply.
&emsp;&emsp;[vue-quasar-admin](http://jaycewu.coding.me/vue-quasar-admin) is a set of back-end management system based on Quasar-Framework with universal permission control (currently only for PC) end).

[![](https://ci.appveyor.com/api/projects/status/github/wjkang/vue-quasar-admin?branch=master&svg=true)]()
[![vue](https://img.shields.io/badge/vue-2.5.16-brightgreen.svg)](https://github.com/vuejs/vue)
[![quasar framework](https://img.shields.io/badge/quasar-0.15.14-brightgreen.svg)](https://quasar-framework.org/)
[![MIT](https://img.shields.io/badge/license-MIT-brightgreen.svg)]()

[online demo] (http://jaycewu.coding.me/vue-quasar-admin)

Login account:

    Admin 123

    Test 123456

    Website_admin 123456

Please do not modify the account name at will, other operations are free, you can initialize the data through the "data initialization" button in the upper right corner.

## System flow chart

![](https://raw.githubusercontent.com/wjkang/vue-quasar-admin/master/screenshot/flowchart.png)

## Features and Features

- Real backend data support
- Login / Logout
- Flexible nine-square grid layout
- Shrink the left menu bar
- tag tag navigation
- Bread crumbs
- Full screen / exit full screen
- Dynamic menu
- Menu is divided by module
- Universal access control
    - Menu level access control
    - Interface level access control
    - Element level access control
- Global configurable loading effect
- Network exception handling
- Module
    - System module
        - System settings
            - Menu management
        - authority management
            - Function management
            - Role management
            - Role rights management
            - Role User Management
            - User role management
        - Organization
            - Department management
            - Position management
        - User Management 
    - Website module
        - CMS
            - Article management
    - Development module
        - official components
            - . . .
        - Business components
            - sku
    - Audit log
    - Data initialization

## File Structure
```shell
.
├── .quasar Quasar CLI generated configuration
└── src
    ├── assets resource file
    ├── components custom components
    ├── css style file
    ├──layout layout component
    ├── libs tool method
    ├── router routing configuration
    ├── store state management
    ├── service API management
    ├── plugins components, directives, plugins that require global registration
    └── pages
        ├── cms
        │ └── Article Management
        ├──develop
        │ ├── Official components
        │ └── Business Components
        ├── organization
        │ ├── Department Management
        │ └── Job Management
        ├── other
        │ └── Audit log
        ├── permission
        │ ├── Function Management
        │ ├── Role Management
        │ ├── Role rights management
        │ ├── Role User Management
        │ └── User Role Management
        ├── system
        │ ├── Menu Management
        ├── user
        │ └── User Management
        ├── 403 No permission page
        ├── index Home
        └── login login page
        
```

## Installation and use

## Install
```bush
Npm install -g vue-cli
```
```bush
Npm install -g quasar-cli
```
```bush
Npm install
```
## Run
### Development
```bush
Quasar dev
```
### Production(Build)
```bush
Quasar build
```

### Install daemon


[Background Program] (https://github.com/wjkang/quasar-admin-server)

```bush
Git clone https://github.com/wjkang/quasar-admin-server.git
```

## Install
```bush
Npm install
```
## Run
### Development
```bush
Npm run start
```
### Production(Build)
```bush
Npm run production
```
The backend program is built using [koa2] (https://github.com/koajs/koa) and uses [lowdb](https://github.com/typicode/lowdb) to persist data to JSON files (using JSON files) Storage is to build a demo quickly).

## Functional development steps (take article management as an example)
- front end
    - Define function code:
    - post_view - article list view
    - post_edit - article editor
    - post_del - article deletion
    - New article list page post.vue
    - Add a new route
    - Use the menu management function to add the related menu of "Article Management". The menu name must be the same as the name field of the route added in the previous step. The permission code fills in the permission code corresponding to the defined "Article List View" function (menu level permission control)
    - Use the function management to enter the defined function name and function code under the newly created menu.
    - Configuring roles and users
    - Set function permissions for the corresponding role in role rights management
- rear end
    - db.json file added article storage structure
    - Add postService.js under services, write the operation on the db.json file
    - Added post.js under controllers, introduced postService.js to do related operations
    - main-routes.js Add related routes, use PermissionCheck middleware for backend interface level access control (function code or role code can be used)
- front end
    - Add post.js under service, configure API related operations, configure loading field to implement custom loading effect, permit field can configure function encoding and role encoding (implement front-end interface level permission control)
    - Go back to the post.vue file, import API access files, write business code
    - Use the v-permission directive to control whether page elements are displayed, using function encoding and role encoding (implementing element-level permission control)
    - configure dontCache under the store app module to control whether the page is cached
    
Can view the source code in more detail

## Show results

![image](https://raw.githubusercontent.com/wjkang/vue-quasar-admin/master/screenshot/1.jpg)

![image](https://raw.githubusercontent.com/wjkang/vue-quasar-admin/master/screenshot/2.jpg)

![image](https://raw.githubusercontent.com/wjkang/vue-quasar-admin/master/screenshot/3.jpg)

![image](https://raw.githubusercontent.com/wjkang/vue-quasar-admin/master/screenshot/4.jpg)

![image](https://raw.githubusercontent.com/wjkang/vue-quasar-admin/master/screenshot/5.jpg)

![image](https://raw.githubusercontent.com/wjkang/vue-quasar-admin/master/screenshot/6.jpg)

![image](https://raw.githubusercontent.com/wjkang/vue-quasar-admin/master/screenshot/7.jpg)

![image](https://raw.githubusercontent.com/wjkang/vue-quasar-admin/master/screenshot/8.jpg)

![image](https://raw.githubusercontent.com/wjkang/vue-quasar-admin/master/screenshot/9.jpg)

![image](https://raw.githubusercontent.com/wjkang/vue-quasar-admin/master/screenshot/10.jpg)

![image](https://raw.githubusercontent.com/wjkang/vue-quasar-admin/master/screenshot/11.jpg)

![image](https://raw.githubusercontent.com/wjkang/vue-quasar-admin/master/screenshot/12.jpg)

![image](https://raw.githubusercontent.com/wjkang/vue-quasar-admin/master/screenshot/13.jpg)


